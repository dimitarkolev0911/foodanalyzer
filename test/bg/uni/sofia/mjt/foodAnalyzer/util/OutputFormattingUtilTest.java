package bg.uni.sofia.mjt.foodAnalyzer.util;

import bg.uni.sofia.mjt.foodAnalyzer.dto.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class OutputFormattingUtilTest {

    @Test
    public void testFormatSearchingByFoodNameWithCorrectData() {
        Food food = new Food("Description 1", "Data Type", 123, "00123456");
        Food food2 = new Food("Description 2", "Branded", 124, "00123457");
        FoodsResult foodsResult = new FoodsResult(new Food[]{food, food2});
        String output = OutputFormattingUtil.formatSearchingByFoodName(foodsResult);
        int rowWidth = 46;
        String[] rows = output.split(System.lineSeparator());

        assertEquals(6, rows.length);
        assertEquals("|  ID |   Description | Data Type | Gtin Upc |", rows[0]);
        assertEquals("-".repeat(rowWidth), rows[1]);
        assertEquals("| 123 | Description 1 | Data Type | 00123456 |", rows[2]);
        assertEquals("-".repeat(rowWidth), rows[3]);
        assertEquals("| 124 | Description 2 |   Branded | 00123457 |", rows[4]);
        assertEquals("-".repeat(rowWidth), rows[5]);
    }

    @Test
    public void testFormatFoodOutputNameWithCorrectData() {
        Food food = new Food("Description 1", "Data Type", 123, "00123456");
        String output = OutputFormattingUtil.formatFoodOutput(food);
        int rowWidth = 46;
        String[] rows = output.split(System.lineSeparator());

        assertEquals(4, rows.length);
        assertEquals("|  ID |   Description | Data Type | Gtin Upc |", rows[0]);
        assertEquals("-".repeat(rowWidth), rows[1]);
        assertEquals("| 123 | Description 1 | Data Type | 00123456 |", rows[2]);
        assertEquals("-".repeat(rowWidth), rows[3]);
    }

    @Test
    public void testFormatResultFromDetailedSearchWithCorrectData() {
        int fdcId = 12345678;
        String ingredients = "Ingredients : Milk, Banana";
        String description = "Banana milk shake";
        Nutrient fat = new Nutrient(5);
        Nutrient protein = new Nutrient(20);
        Nutrient carbs = new Nutrient(20);
        Nutrient fibers = new Nutrient(6);
        Nutrient calories = new Nutrient(205);
        FoodNutrients foodNutrients = new FoodNutrients(fat, protein, carbs, calories, fibers);
        DetailedFoodInfo detailedFoodInfo = new DetailedFoodInfo(fdcId, ingredients, description, foodNutrients);
        int rowWidth = 122;

        String output = OutputFormattingUtil.formatResultFromDetailedSearch(detailedFoodInfo);
        String[] rows = output.split(System.lineSeparator());

        assertEquals(4, rows.length);
        assertEquals("|    FdcId |   Product Name    | Ingredients  |     Calories | Proteins (g) |     Fats (g) |    Carbs (g) |   Fibers (g) |", rows[0]);
        assertEquals("-".repeat(rowWidth), rows[1]);
        assertEquals("| 12345678 | Banana milk shake | Milk, Banana |       205.00 |        20.00 |         5.00 |        20.00 |         6.00 |", rows[2]);
        assertEquals("-".repeat(rowWidth), rows[3]);
    }

}
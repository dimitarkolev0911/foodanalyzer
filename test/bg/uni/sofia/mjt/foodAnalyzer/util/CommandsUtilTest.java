package bg.uni.sofia.mjt.foodAnalyzer.util;

import bg.uni.sofia.mjt.foodAnalyzer.models.Command;
import bg.uni.sofia.mjt.foodAnalyzer.models.CommandEnum;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommandsUtilTest {

    @Test(expected = NullPointerException.class)
    public void testGetCommandByUserInputWhenLineIsNull() {
        CommandsUtil.getCommandFormatByUserInput(null);
    }

    @Test
    public void testGetCommandFormatByUserInputWhenLineContainsOnlyOneWord() {
        String line = "get-food";
        Command command = CommandsUtil.getCommandFormatByUserInput(line);

        assertEquals(line, command.command());
        assertEquals("", command.argument());
    }

    @Test
    public void testGetCommandFormatByUserInputWhenLineContainsTwoWords() {
        String line = "get-food Apple";
        Command command = CommandsUtil.getCommandFormatByUserInput(line);

        assertEquals("get-food", command.command());
        assertEquals("Apple", command.argument());
    }

    @Test
    public void testGetCommandFormatUseInputWhenLineContainsMoreThanTwoWords() {
        String line = "get-food Apple Oreo";
        Command command = CommandsUtil.getCommandFormatByUserInput(line);

        assertEquals("get-food", command.command());
        assertEquals("Apple Oreo", command.argument());
    }

    @Test(expected = NullPointerException.class)
    public void testGetCommandFromUserInputWhenFormatIsNull() {
        CommandsUtil.getCommandFromUserInput(null);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetInfoAndArgumentIsNull() {
        Command format = new Command("get-food", null);
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_FOOD_WRONG, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetInfoAndArgumentIsOnlyFromWhitespaces() {
        Command format = new Command("get-food", "   ");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_FOOD_WRONG, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetInfoAndArgumentIsValid() {
        Command format = new Command("get-food", "Apple");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_FOOD, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetFoodReportAndArgumentIsNull() {
        Command format = new Command("get-food-report", null);
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_REPORT_WRONG, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetFoodReportAndArgumentIsOnlyFromWhitespaces() {
        Command format = new Command("get-food-report", "   ");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_REPORT_WRONG, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetReportInfoAndArgumentIsValid() {
        Command format = new Command("get-food-report", "Apple");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_REPORT, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetFoodByBarcodeAndArgumentIsNull() {
        Command format = new Command("get-food-by-barcode", null);
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_BY_BARCODE_WRONG, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetFoodByBarcodeAndArgumentIsOnlyFromWhitespaces() {
        Command format = new Command("get-food-by-barcode", "   ");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_BY_BARCODE_WRONG, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetFoodByBarcodeAndArgumentDoesntStartWithDashDashCode() {
        Command format = new Command("get-food-by-barcode", "code");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_BY_BARCODE_WRONG, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetFoodByBarcodeAndArgumentIsCode() {
        Command format = new Command("get-food-by-barcode", "--code=1234");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_BY_BARCODE, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsGetFoodByBarcodeAndArgumentIsImg() {
        Command format = new Command("get-food-by-barcode", "--img=image");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.GET_BY_BARCODE_IMAGE, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsDisconnect() {
        Command format = new Command("disconnect", "");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.DISCONNECT, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsHelp() {
        Command format = new Command("help", "");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.HELP, commandEnum);
    }

    @Test
    public void testGetCommandFromUserInputWhenCommandIsUnknown() {
        Command format = new Command("unknown", "123");
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(format);

        assertEquals(CommandEnum.DEFAULT, commandEnum);
    }

}

package bg.uni.sofia.mjt.foodAnalyzer.util;

import com.google.zxing.NotFoundException;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class BarcodeUtilTest {

    @Test
    public void testGetBarcodeByImage() {
        String path = "resources/test/images/upc-barcode.gif";

        try {
            String gtinUpc = BarcodeUtil.getBarcodeByImage(path);
            assertEquals("009800146130", gtinUpc);
        } catch (IOException | NotFoundException ioException) {
            fail("There is an error with extracting gtinUpc");
            ioException.printStackTrace();
        }

    }

}
package bg.uni.sofia.mjt.foodAnalyzer.util;

import org.junit.Test;

import java.net.URI;

import static org.junit.Assert.*;

public class UriUtilTest {

    private final String API_KEY = "123";
    private final String TEMPLATE_FOR_KEYWORD = "api.test/input=%s&api_key=%s";
    private final String TEMPLATE_FOR_ID = "api.test/id=%d&api_key=%s";

    @Test
    public void testPrepareUrlForSearchingFoodByKeyWordWithOneWord() {
        String keyWord = "Milka";
        URI uri = UriUtil.prepareUrlForSearchingFoodByKeyWord(TEMPLATE_FOR_KEYWORD, keyWord, API_KEY);

        assertNotNull(uri);
        assertEquals("api.test/input=Milka&api_key=123", uri.getPath());
    }

    @Test
    public void testPrepareUrlForSearchingFoodByKeyWordWithMoreThanOneWord() {
        String keyWord = "Milka Oreo";
        URI uri = UriUtil.prepareUrlForSearchingFoodByKeyWord(TEMPLATE_FOR_KEYWORD, keyWord, API_KEY);

        assertNotNull(uri);
        assertEquals("api.test/input=Milka%20Oreo&api_key=123", uri.getRawPath());
        assertEquals("api.test/input=Milka Oreo&api_key=123", uri.getPath());
    }

    @Test
    public void testPrepareUrlForSearchingFoodById() {
        int id = 987;
        URI uri = UriUtil.prepareUrlForSearchingFoodById(TEMPLATE_FOR_ID, id, API_KEY);

        assertNotNull(uri);
        assertEquals("api.test/id=987&api_key=123", uri.getPath());
    }

}
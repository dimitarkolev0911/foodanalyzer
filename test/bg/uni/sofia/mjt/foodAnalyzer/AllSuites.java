package bg.uni.sofia.mjt.foodAnalyzer;

import bg.uni.sofia.mjt.foodAnalyzer.cache.FoodCacheTest;
import bg.uni.sofia.mjt.foodAnalyzer.services.SearchFoodServiceImplTest;
import bg.uni.sofia.mjt.foodAnalyzer.util.BarcodeUtilTest;
import bg.uni.sofia.mjt.foodAnalyzer.util.CommandsUtilTest;
import bg.uni.sofia.mjt.foodAnalyzer.util.OutputFormattingUtilTest;
import bg.uni.sofia.mjt.foodAnalyzer.util.UriUtilTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        CommandsUtilTest.class,
        OutputFormattingUtilTest.class,
        UriUtilTest.class,
        BarcodeUtilTest.class,
        FoodCacheTest.class,
        SearchFoodServiceImplTest.class
})

public class AllSuites {
}

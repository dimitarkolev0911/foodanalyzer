package bg.uni.sofia.mjt.foodAnalyzer.cache;

import bg.uni.sofia.mjt.foodAnalyzer.dto.Food;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.*;

import java.io.*;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static bg.uni.sofia.mjt.foodAnalyzer.constants.Constants.CACHE_TEST_DIRECTORY;
import static org.junit.Assert.*;

public class FoodCacheTest {

    private FoodCache foodCache;
    private final static String gtinUpc = "12345678";
    private final static String gtinUpc2 = "12345679";
    private final static String SEPARATOR = File.separator;

    @Before
    public void setUp() throws IOException {
        Food food = new Food("Description 1", "Branded", 12345678, gtinUpc);
        Food food2 = new Food("Description 2", "Branded", 12345679, gtinUpc2);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Writer writer = new FileWriter(CACHE_TEST_DIRECTORY + SEPARATOR + gtinUpc + ".json");
        Writer writer2 = new FileWriter(CACHE_TEST_DIRECTORY + SEPARATOR + gtinUpc2 + ".json");

        gson.toJson(food, writer);
        gson.toJson(food2, writer2);
        writer.close();
        writer2.close();
        foodCache = new FoodCache(CACHE_TEST_DIRECTORY);
    }

    @Test
    public void testIfPopulatingTheSetInTheDatabaseIsCorrect() {
        Set<String> gtinUpcs = foodCache.getFoods();

        assertEquals(2, gtinUpcs.size());
        assertTrue(gtinUpcs.containsAll(Arrays.asList(gtinUpc, gtinUpc2)));
    }

    @Test
    public void testIfAddNewFoodsToCacheWorksProperlyWhenGivenEmptyArray() throws IOException {
        foodCache.addNewFoodsToCache(new Food[]{});

        testIfPopulatingTheSetInTheDatabaseIsCorrect();
    }

    @Test
    public void testIfAddNewFoodsToCacheWorksProperlyWhenGivenOnlyOneDuplicatedItem() throws IOException {
        Food food = new Food("Description 1", "Branded", 12345678, gtinUpc);

        foodCache.addNewFoodsToCache(new Food[]{food});

        testIfPopulatingTheSetInTheDatabaseIsCorrect();
    }

    @Test
    public void testIfAddNewFoodsToCacheWorksProperlyWhenAddingOneDuplicatedAndOneNewItem() throws IOException {

        Food duplicatedFood = new Food("Description 1", "Branded", 12345678, gtinUpc);
        Food newFood = new Food("Description 3", "Branded", 12345680, "12345680");

        foodCache.addNewFoodsToCache(new Food[]{duplicatedFood, newFood});

        Set<String> gtinUpcs = foodCache.getFoods();

        assertEquals(3, gtinUpcs.size());
        assertTrue(gtinUpcs.containsAll(Arrays.asList(gtinUpc, gtinUpc2, "12345680")));
    }

    @Test
    public void testIfGetFoodByIdWorkCorrectWhenSearchingForValidFood() throws IOException {
        Food food = null;
        try {
            food = foodCache.getFoodById(gtinUpc);
        } catch (FileNotFoundException e) {
            fail("The file was not found!");
            e.printStackTrace();
        }
        assertNotNull(food);
        assertEquals("Description 1", food.getDescription());
        assertEquals("Branded", food.getDataType());
        assertEquals(gtinUpc, food.getGtinUpc());
        assertEquals(12345678, food.getFdcId());
    }

    @Test
    public void testIfGetFoodByIdWorkCorrectWhenSearchingForFoodWhichIsNotInCache() throws IOException {
        Food food = new Food();
        try {
            food = foodCache.getFoodById("12345680");
        } catch (FileNotFoundException e) {
            fail("The file was not found!");
            e.printStackTrace();
        }
        assertNull(food);
    }

    @After
    public void cleanUpFiles() {
        Set<String> fileNames = fileNamesInDirectory();

        for(String fileName: fileNames) {
            File file = new File(CACHE_TEST_DIRECTORY + File.separator + fileName);

            if(file.exists() && file.delete()) {
                System.out.println(file.getPath() + " deleted successfully");
            }
        }
    }

    private Set<String> fileNamesInDirectory() {
        return Arrays.stream(Objects.requireNonNull(new File(CACHE_TEST_DIRECTORY).listFiles()))
                .map(File::getName)
                .collect(Collectors.toSet());
    }


}
package bg.uni.sofia.mjt.foodAnalyzer.services;

import bg.uni.sofia.mjt.foodAnalyzer.builders.FoodNutrientBuilder;
import bg.uni.sofia.mjt.foodAnalyzer.dto.*;
import bg.uni.sofia.mjt.foodAnalyzer.errors.HttpResponseError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.FieldPosition;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static bg.uni.sofia.mjt.foodAnalyzer.constants.Constants.CACHE_TEST_DIRECTORY;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SearchFoodServiceImplTest {

    @Mock
    HttpResponse<String> response;

    @Mock
    HttpClient httpClient;

    private SearchFoodService searchFoodService;
    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private final String API_KEY = "12345";

    @Before
    public void setUp() {
        searchFoodService = new SearchFoodServiceImpl(API_KEY, httpClient, CACHE_TEST_DIRECTORY);
    }

    @Test
    public void testGetFoodByName() throws IOException, InterruptedException, HttpResponseError {
        Food food = new Food();
        food.setGtinUpc("12345678");
        food.setDataType("Branded");
        food.setDescription("Description 1");
        food.setFdcId(12345678);
        FoodsResult mockedResult = new FoodsResult();
        mockedResult.setFoods(new Food[]{food});

        when(response.body()).thenReturn(gson.toJson(mockedResult));
        when(httpClient.send(any(), eq(HttpResponse.BodyHandlers.ofString()))).thenReturn(response);

        FoodsResult result = searchFoodService.getFoodByName("Milka");

        assertNotNull(result);
        assertNotNull(result.getFoods());
        assertEquals(mockedResult, result);
        assertEquals(mockedResult.hashCode(), result.hashCode());

        Food[] foods = result.getFoods();

        assertEquals(1, foods.length);
        assertEquals(food.getFdcId(), foods[0].getFdcId());
        assertEquals(food.getGtinUpc(), foods[0].getGtinUpc());
        assertEquals(food.getDataType(), foods[0].getDataType());
        assertEquals(food.getDescription(), foods[0].getDescription());
        assertEquals(food, foods[0]);
        assertEquals(food.hashCode(), foods[0].hashCode());
    }

    @Test(expected = HttpResponseError.class)
    public void testGetFoodByNameWhenExceptionIsThrown() throws IOException, InterruptedException, HttpResponseError {
        when(httpClient.send(any(), eq(HttpResponse.BodyHandlers.ofString()))).thenThrow(IOException.class);
        searchFoodService.getFoodByName("Milka");
    }

    @Test
    public void testGetDetailedFoodInfoById() throws IOException, InterruptedException, HttpResponseError {
        FoodNutrients nutrients = new FoodNutrientBuilder().withFats(5.0)
                .withCarbs(10.0)
                .withProteins(10.0)
                .withFibers(5.0)
                .withCalories(125.0).build();
        DetailedFoodInfo detailedFoodInfo = new DetailedFoodInfo();
        detailedFoodInfo.setIngredients("Banana, Milk");
        detailedFoodInfo.setNutrients(nutrients);
        detailedFoodInfo.setDescription("Banana Milk Shake");
        detailedFoodInfo.setFdcId(12345678);

        when(response.body()).thenReturn(gson.toJson(detailedFoodInfo));
        when(httpClient.send(any(), eq(HttpResponse.BodyHandlers.ofString()))).thenReturn(response);

        DetailedFoodInfo responseFood = searchFoodService.getDetailedFoodInfoById(12345678);

        assertNotNull(responseFood);
        assertEquals(detailedFoodInfo.getIngredients(), responseFood.getIngredients());
        assertEquals(detailedFoodInfo.getDescription(), responseFood.getDescription());
        assertEquals(detailedFoodInfo.getFdcId(), responseFood.getFdcId());
    }

    @Test
    public void testGetFoodByGtinUpcWhenCodeIsPresent() throws InterruptedException, HttpResponseError, IOException {
        Food food = new Food();
        food.setGtinUpc("12345678");
        food.setDataType("Branded");
        food.setDescription("Milka");
        food.setFdcId(12345678);

        when(response.body()).thenReturn(gson.toJson(new FoodsResult(new Food[]{food})));
        when(httpClient.send(any(), eq(HttpResponse.BodyHandlers.ofString()))).thenReturn(response);

        FoodsResult result = searchFoodService.getFoodByName("Milka");

        assertNotNull(result);
        assertNotNull(result.getFoods());

        Food[] foods = result.getFoods();

        assertEquals(1, foods.length);

        Food searchedFood = searchFoodService.getFoodByGtinUpcCode("12345678");

        assertNotNull(searchedFood);
        assertEquals(food.getDescription(), searchedFood.getDescription());
        assertEquals(food.getGtinUpc(), searchedFood.getGtinUpc());
        assertEquals(food.getFdcId(), searchedFood.getFdcId());
        assertEquals(food.getDataType(), searchedFood.getDataType());
    }

    @After
    public void cleanUp() {
        Set<String> fileNames = fileNamesInDirectory();

        for(String fileName: fileNames) {
            File file = new File(CACHE_TEST_DIRECTORY + File.separator + fileName);

            if(file.exists() && file.delete()) {
                System.out.println(file.getPath() + " deleted successfully");
            }
        }
    }

    private Set<String> fileNamesInDirectory() {
        return Arrays.stream(Objects.requireNonNull(new File(CACHE_TEST_DIRECTORY).listFiles()))
                .map(File::getName)
                .collect(Collectors.toSet());
    }

}
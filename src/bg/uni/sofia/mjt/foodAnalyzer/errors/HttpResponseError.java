package bg.uni.sofia.mjt.foodAnalyzer.errors;

public class HttpResponseError  extends Exception {

    public HttpResponseError(Throwable cause) {
        super(cause);
    }
}

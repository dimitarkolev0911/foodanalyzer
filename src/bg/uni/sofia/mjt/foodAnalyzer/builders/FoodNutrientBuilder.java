package bg.uni.sofia.mjt.foodAnalyzer.builders;

import bg.uni.sofia.mjt.foodAnalyzer.dto.FoodNutrients;
import bg.uni.sofia.mjt.foodAnalyzer.dto.Nutrient;

public class FoodNutrientBuilder {

    private FoodNutrients foodNutrients;

    public FoodNutrientBuilder() {
        foodNutrients = new FoodNutrients(new Nutrient(0.0),new Nutrient(0.0),new Nutrient(0.0)
                ,new Nutrient(0.0),new Nutrient(0.0));
    }

    public FoodNutrientBuilder withFats(double value) {
        Nutrient fat = new Nutrient(value);
        foodNutrients.setFat(fat);
        return this;
    }

    public FoodNutrientBuilder withCarbs(double value) {
        Nutrient carbs = new Nutrient();
        carbs.setValue(value);
        foodNutrients.setCarbohydrates(carbs);
        return this;
    }

    public FoodNutrientBuilder withFibers(double value) {
        Nutrient fibers = new Nutrient(value);
        foodNutrients.setFibers(fibers);
        return this;
    }

    public FoodNutrientBuilder withProteins(double value) {
        Nutrient proteins = new Nutrient(value);
        foodNutrients.setProtein(proteins);
        return this;
    }

    public FoodNutrientBuilder withCalories(double value) {
        Nutrient calories = new Nutrient(value);
        foodNutrients.setCalories(calories);
        return this;
    }

    public FoodNutrients build() {
        return this.foodNutrients;
    }
}

package bg.uni.sofia.mjt.foodAnalyzer.models;

public record Command(String command, String argument) {

}

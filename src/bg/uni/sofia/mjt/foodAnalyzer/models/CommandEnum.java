package bg.uni.sofia.mjt.foodAnalyzer.models;

public enum CommandEnum {

    GET_FOOD,
    GET_FOOD_WRONG,
    GET_REPORT,
    GET_REPORT_WRONG,
    GET_BY_BARCODE,
    GET_BY_BARCODE_WRONG,
    GET_BY_BARCODE_IMAGE,
    HELP,
    DISCONNECT,
    DEFAULT

}

package bg.uni.sofia.mjt.foodAnalyzer.dto;

public class Nutrient {

    private double value;

    public Nutrient() {
        value = 0;
    }

    public Nutrient(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}

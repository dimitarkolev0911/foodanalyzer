package bg.uni.sofia.mjt.foodAnalyzer.dto;

import com.google.gson.annotations.SerializedName;

public class DetailedFoodInfo extends BaseFoodDetails {

    @SerializedName("ingredients")
    private String ingredients;
    @SerializedName("labelNutrients")
    private FoodNutrients labelNutrients;

    public DetailedFoodInfo() {
        super();
        this.ingredients = "";
        this.labelNutrients = new FoodNutrients();
    }

    public DetailedFoodInfo(int fdcId, String ingredients, String description, FoodNutrients nutrients) {
        super(description, fdcId);
        this.ingredients = ingredients;
        this.labelNutrients = nutrients;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public FoodNutrients getNutrients() {
        return labelNutrients;
    }

    public void setNutrients(FoodNutrients nutrients) {
        this.labelNutrients = nutrients;
    }
}

package bg.uni.sofia.mjt.foodAnalyzer.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseFoodDetails implements Serializable {

    @SerializedName("description")
    private String description;

    @SerializedName("fdcId")
    private int fdcId;

    public BaseFoodDetails() {
        description = "";
        fdcId = 0;
    }

    public BaseFoodDetails(String description, int fdcId) {
        this.description = description;
        this.fdcId = fdcId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getFdcId() {
        return fdcId;
    }

    public void setFdcId(int fdcId) {
        this.fdcId = fdcId;
    }
}

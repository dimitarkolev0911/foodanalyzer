package bg.uni.sofia.mjt.foodAnalyzer.dto;

public class FoodNutrients {

    private Nutrient fat;
    private Nutrient protein;
    private Nutrient carbohydrates;
    private Nutrient calories;
    private Nutrient fiber;

    public FoodNutrients() {
    }

    public FoodNutrients(Nutrient fat, Nutrient protein, Nutrient carbohydrates, Nutrient calories, Nutrient fibers) {
        this.fat = fat;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.calories = calories;
        this.fiber = fibers;
    }

    public Nutrient getFat() {
        return fat;
    }

    public void setFat(Nutrient fat) {
        this.fat = fat;
    }

    public Nutrient getProtein() {
        return protein;
    }

    public void setProtein(Nutrient protein) {
        this.protein = protein;
    }

    public Nutrient getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(Nutrient carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public Nutrient getCalories() {
        return calories;
    }

    public void setCalories(Nutrient calories) {
        this.calories = calories;
    }

    public Nutrient getFibers() {
        return fiber;
    }

    public void setFibers(Nutrient fibers) {
        this.fiber = fibers;
    }
}

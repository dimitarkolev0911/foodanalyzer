package bg.uni.sofia.mjt.foodAnalyzer.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class Food extends BaseFoodDetails implements Serializable {

    @SerializedName("dataType")
    private String dataType;

    @SerializedName("gtinUpc")
    private String gtinUpc;

    public Food() {
        super();
        dataType = "";
        gtinUpc = "";
    }

    public Food(String description, String dataType, int fdcId, String gtinUpc) {
        super(description, fdcId);
        this.dataType = dataType;
        this.gtinUpc = gtinUpc;
    }

    public String getDataType() {
        return dataType;
    }

    public String getGtinUpc() {
        return gtinUpc;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public void setGtinUpc(String gtinUpc) {
        this.gtinUpc = gtinUpc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Food food = (Food) o;
        return getFdcId() == food.getFdcId() &&
                getDescription().equals(food.getDescription()) &&
                dataType.equals(food.dataType) &&
                Objects.equals(gtinUpc, food.gtinUpc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDescription(), dataType, getFdcId(), gtinUpc);
    }
}

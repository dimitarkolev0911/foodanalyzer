package bg.uni.sofia.mjt.foodAnalyzer.dto;

import java.util.Arrays;

public class FoodsResult {

    private Food[] foods;

    public FoodsResult() {
        foods = new Food[10];
    }

    public FoodsResult(Food[] foods) {
        this.foods = foods;
    }

    public void setFoods(Food[] foods) {
        this.foods = foods;
    }

    public Food[] getFoods() {
        return foods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FoodsResult foodsResult = (FoodsResult) o;
        return Arrays.equals(foods, foodsResult.foods);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(foods);
    }
}

package bg.uni.sofia.mjt.foodAnalyzer.services;

import bg.uni.sofia.mjt.foodAnalyzer.dto.DetailedFoodInfo;
import bg.uni.sofia.mjt.foodAnalyzer.dto.Food;
import bg.uni.sofia.mjt.foodAnalyzer.dto.FoodsResult;
import bg.uni.sofia.mjt.foodAnalyzer.errors.HttpResponseError;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface SearchFoodService {

    FoodsResult getFoodByName(String foodName) throws HttpResponseError, IOException;

    DetailedFoodInfo getDetailedFoodInfoById(int id) throws IOException, HttpResponseError;

    Food getFoodByGtinUpcCode(String gtinUpc) throws IOException;
}

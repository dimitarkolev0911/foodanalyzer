package bg.uni.sofia.mjt.foodAnalyzer.services;

import bg.uni.sofia.mjt.foodAnalyzer.cache.FoodCache;
import bg.uni.sofia.mjt.foodAnalyzer.dto.DetailedFoodInfo;
import bg.uni.sofia.mjt.foodAnalyzer.dto.Food;
import bg.uni.sofia.mjt.foodAnalyzer.dto.FoodsResult;
import bg.uni.sofia.mjt.foodAnalyzer.errors.HttpResponseError;
import bg.uni.sofia.mjt.foodAnalyzer.util.UriUtil;
import com.google.gson.Gson;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


public class SearchFoodServiceImpl implements SearchFoodService {

    private static final String SEARCH_FOOD_BY_NAME_URL = "https://api.nal.usda.gov/fdc/v1/search?generalSearch" +
            "Input=%s&requireAllWords=true&api_key=%s";
    private static final String SEARCH_FOOD_BY_ID_URL = "https://api.nal.usda.gov/fdc/v1/food/%d?api_key=%s";

    private static final Gson gson = new Gson();
    private final HttpClient httpClient;
    private final FoodCache foodCache;
    private final String apiKey;

    public SearchFoodServiceImpl(String apiKey, HttpClient httpClient, String cacheDirectory) {
        this.apiKey = apiKey;
        this.httpClient = httpClient;
        this.foodCache = new FoodCache(cacheDirectory);
    }

    @Override
    public FoodsResult getFoodByName(String foodName) throws HttpResponseError, IOException {
        URI requestUri = UriUtil.prepareUrlForSearchingFoodByKeyWord(SEARCH_FOOD_BY_NAME_URL, foodName, apiKey);
        HttpResponse<String> response = getResponseByUri(requestUri);
        FoodsResult foodsResult = gson.fromJson(response.body(), FoodsResult.class);
        foodCache.addNewFoodsToCache(foodsResult.getFoods());
        return foodsResult;
    }

    @Override
    public DetailedFoodInfo getDetailedFoodInfoById(int id) throws HttpResponseError {
        URI requestUri = UriUtil.prepareUrlForSearchingFoodById(SEARCH_FOOD_BY_ID_URL, id, apiKey);
        HttpResponse<String> response = getResponseByUri(requestUri);
        return gson.fromJson(response.body(), DetailedFoodInfo.class);
    }

    @Override
    public Food getFoodByGtinUpcCode(String gtinUpc) throws IOException {
        return foodCache.getFoodById(gtinUpc);
    }

    private HttpResponse<String> getResponseByUri(URI requestUri) throws HttpResponseError {
        HttpRequest request = HttpRequest.newBuilder().GET().uri(requestUri).build();
        HttpResponse<String> response;
        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException ioException) {
            throw new HttpResponseError(ioException);
        }
        return response;
    }
}

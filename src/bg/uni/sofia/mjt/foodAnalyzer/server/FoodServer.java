package bg.uni.sofia.mjt.foodAnalyzer.server;

import bg.uni.sofia.mjt.foodAnalyzer.dto.Food;
import bg.uni.sofia.mjt.foodAnalyzer.errors.HttpResponseError;
import bg.uni.sofia.mjt.foodAnalyzer.models.Command;
import bg.uni.sofia.mjt.foodAnalyzer.models.CommandEnum;
import bg.uni.sofia.mjt.foodAnalyzer.services.SearchFoodServiceImpl;
import bg.uni.sofia.mjt.foodAnalyzer.util.BarcodeUtil;
import bg.uni.sofia.mjt.foodAnalyzer.util.CommandsUtil;
import bg.uni.sofia.mjt.foodAnalyzer.util.OutputFormattingUtil;
import com.google.zxing.NotFoundException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.http.HttpClient;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Set;

import static bg.uni.sofia.mjt.foodAnalyzer.constants.Constants.*;

public class FoodServer implements AutoCloseable {
    private static final int SERVER_PORT = 7777;
    private static final int BUFFER_SIZE = 65536;
    private static final String NEW_LINE = System.lineSeparator();

    private Selector selector;
    private ByteBuffer buffer;
    private ServerSocketChannel serverSocketChannel;
    private boolean isRunning = true;
    private SearchFoodServiceImpl searchFoodService;

    public FoodServer(int serverPort) {
        try {
            searchFoodService = new SearchFoodServiceImpl(API_KEY, HttpClient.newBuilder().build(), CACHE_DIRECTORY);
            selector = Selector.open();
            buffer = ByteBuffer.allocate(BUFFER_SIZE);
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.socket().bind(new InetSocketAddress(serverPort));
        } catch (IOException e) {
            System.err.println(ERROR_WITH_INITIALIZING_THE_SERVER);
            e.printStackTrace();
        }
    }

    public void start() {
        try {
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            while (isRunning) {
                int readyChannels = selector.select();
                if (readyChannels <= 0) {
                    System.out.println(NOTHING_TO_READ_MESSAGE);
                    continue;
                }

                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
                while (keyIterator.hasNext()) {
                    SelectionKey key = keyIterator.next();
                    if (key.isReadable()) {
                        this.readKey(key);
                    } else if (key.isAcceptable()) {
                        this.acceptKey(key);
                    }
                    keyIterator.remove();
                }
            }
        } catch (IOException e) {
            stop();
            System.err.println(ERROR_WHILE_RUNNING_THE_SERVER);
            e.printStackTrace();
        }
    }

    private void stop() {
        isRunning = false;
        if (selector.isOpen()) {
            selector.wakeup();
        }
    }

    private void acceptKey(SelectionKey key) {
        try {
            ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
            SocketChannel socketChannel = serverSocketChannel.accept();
            socketChannel.configureBlocking(false);
            socketChannel.register(selector, SelectionKey.OP_READ);
        } catch (IOException e) {
            System.err.println(ERROR_IN_ACCEPTING_SOCKET);
            e.printStackTrace();
        }
    }

    private void readKey(SelectionKey key) {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        try {
            String message = getMessageFromClient(socketChannel);
            if (message == null) {
                return;
            }

            String response;

            try {
                response = generateRespond(message.trim());
            } catch (Exception e) {
                e.printStackTrace();
                response = NO_RESULTS + NEW_LINE;
            }

            writeToBuffer(response);

            sendMessageToClient(socketChannel);
        } catch (IOException e) {
            stop();
            System.err.println(ERROR_WHILE_READING_WRITING_IN_BUFFER);
            e.printStackTrace();
        }
    }

    private String getMessageFromClient(SocketChannel socketChannel) throws IOException {
        buffer.clear();
        int r = socketChannel.read(buffer);
        if (r <= 0) {
            System.out.println(NOTHING_TO_READ_MESSAGE);
            socketChannel.close();
            return null;
        }
        buffer.flip();
        return StandardCharsets.UTF_8.decode(buffer).toString();
    }

    private void writeToBuffer(String reply) {
        buffer.clear();
        try {
            buffer.put((reply + NEW_LINE).getBytes());
        } catch (BufferOverflowException e) {
            String errorMessage = String.format(BUFFER_OVERLOAD_MESSAGE, BUFFER_SIZE);
            System.err.println(errorMessage);
            e.printStackTrace();
        }
        buffer.flip();
    }

    private void sendMessageToClient(SocketChannel socketChannel) throws IOException {
        socketChannel.write(buffer);
        buffer.clear();
    }

    private String generateRespond(String message) {
        message = message.trim();
        Command command = CommandsUtil.getCommandFormatByUserInput(message);
        CommandEnum commandEnum = CommandsUtil.getCommandFromUserInput(command);
        try {
            return generateMessageFromCommand(commandEnum, command);
        } catch (IOException ioException ) {
            return IO_EXCEPTION_MESSAGE + NEW_LINE;
        } catch (HttpResponseError httpResponseError) {
            return HTTP_RESPONSE_MESSAGE + NEW_LINE;
        } catch (NotFoundException notFoundException) {
            return NOT_FOUND_MESSAGE + NEW_LINE;
        }
    }

    private final String generateMessageFromCommand(CommandEnum commandEnum, Command command) throws IOException, HttpResponseError, NotFoundException {
        switch (commandEnum) {
            case GET_FOOD -> {
                return OutputFormattingUtil
                        .formatSearchingByFoodName(searchFoodService.getFoodByName(command.argument()));
            }
            case GET_REPORT -> {
                return OutputFormattingUtil.formatResultFromDetailedSearch(searchFoodService
                        .getDetailedFoodInfoById(Integer.parseInt(command.argument())));
            }
            case GET_BY_BARCODE -> {
                Food food = searchFoodService.getFoodByGtinUpcCode(command.argument().split("=")[1]);
                return food == null ?
                        NO_SUCH_PRODUCT_MESSAGE + NEW_LINE :
                        OutputFormattingUtil.formatFoodOutput(food);
            }
            case GET_BY_BARCODE_IMAGE -> {
                String gtinUpc = BarcodeUtil.getBarcodeByImage(command.argument().split("=")[1]);
                Food food = searchFoodService.getFoodByGtinUpcCode(gtinUpc);
                return food == null ?
                        NO_SUCH_PRODUCT_MESSAGE + NEW_LINE :
                        OutputFormattingUtil.formatFoodOutput(food);
            }
            case GET_FOOD_WRONG -> {
                return GET_INFO_USAGE + NEW_LINE;
            }
            case GET_REPORT_WRONG -> {
                return GET_FOOD_REPORT_USAGE + NEW_LINE;
            }
            case GET_BY_BARCODE_WRONG -> {
                return GET_FOOD_BY_BARCODE_USAGE + NEW_LINE;
            }
            case HELP -> {
                return HELP_MESSAGE + NEW_LINE;
            }
            case DISCONNECT -> {
                return DISCONNECTING + NEW_LINE;
            }
            default -> {
                return UNKNOWN_COMMAND + NEW_LINE;
            }
        }
    }

    @Override
    public void close() {
        try {
            selector.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            try {
                serverSocketChannel.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        FoodServer foodServer = new FoodServer(SERVER_PORT);
        foodServer.start();
        foodServer.run();
    }
}
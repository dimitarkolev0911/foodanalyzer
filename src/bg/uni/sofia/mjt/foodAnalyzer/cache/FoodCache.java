package bg.uni.sofia.mjt.foodAnalyzer.cache;

import bg.uni.sofia.mjt.foodAnalyzer.dto.Food;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.Writer;
import java.io.Reader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.File;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class FoodCache {

    private final Set<String> foods;
    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private final String CACHE_DIRECTORY;
    private final String JSON = ".json";
    private final String DIRECTORY_SPLITTER = "/";
    private final String BRANDED = "Branded";

    public FoodCache(String directory) {
        this.CACHE_DIRECTORY = directory;
        this.foods = populateFoodsCache();
    }

    public void addNewFoodsToCache(Food[] newFoods) throws IOException {
        Map<String, Food> newFoodsMap = new HashMap<>();
        Arrays.stream(newFoods).forEach(food -> {
            if (BRANDED.equals(food.getDataType()) && !this.foods.contains(food.getGtinUpc())) {
                foods.add(food.getGtinUpc());
                newFoodsMap.put(food.getGtinUpc(), food);
            }
        });
        writeFoodsToFiles(newFoodsMap);
    }

    public Food getFoodById(String gtinUpc) throws IOException {
        if (foods.contains(gtinUpc)) {
            String fileName = CACHE_DIRECTORY + DIRECTORY_SPLITTER + gtinUpc + JSON;
            Reader reader = new FileReader(fileName);
            Food food = gson.fromJson(reader, Food.class);
            reader.close();
            return food;
        }
        return null;
    }

    public Set<String> getFoods() {
        return foods;
    }

    private void writeFoodsToFiles(Map<String, Food> newFoods) throws IOException {
        for (Map.Entry<String, Food> foodEntry : newFoods.entrySet()) {
            String fileName = CACHE_DIRECTORY + DIRECTORY_SPLITTER + foodEntry.getKey() + JSON;
            Writer writer = new FileWriter(fileName);
            gson.toJson(foodEntry.getValue(), writer);
            writer.close();
        }
    }

    private Set<String> populateFoodsCache() {
        File directory = new File(CACHE_DIRECTORY);
        return Arrays.stream(Objects.requireNonNull(directory.listFiles()))
                .map(file -> file.getName().split("\\.")[0])
                .collect(Collectors.toSet());
    }

}

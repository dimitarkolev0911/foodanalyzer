package bg.uni.sofia.mjt.foodAnalyzer.client;

import static bg.uni.sofia.mjt.foodAnalyzer.constants.Constants.PROMPT;
import static bg.uni.sofia.mjt.foodAnalyzer.constants.Constants.DISCONNECT;
import static bg.uni.sofia.mjt.foodAnalyzer.constants.Constants.ERROR_WHILE_READING_INPUT;
import static bg.uni.sofia.mjt.foodAnalyzer.constants.Constants.SERVER_HOST;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.nio.channels.Channels;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class FoodClient {

    private static final int SERVER_PORT = 7777;

    public FoodClient() {
    }

    private void startClient() {
        try (SocketChannel socketChannel = SocketChannel.open();
             BufferedReader reader = new BufferedReader(Channels.newReader(socketChannel, StandardCharsets.UTF_8));
             PrintWriter printWriter =
                     new PrintWriter(Channels.newWriter(socketChannel, StandardCharsets.UTF_8), true);
             Scanner scanner = new Scanner(System.in)) {

            socketChannel.connect(new InetSocketAddress(SERVER_HOST, SERVER_PORT));

            while (true) {
                System.out.print(PROMPT);
                String command = scanner.nextLine();

                printWriter.println(command);
                String line;

                while((line = reader.readLine()) != null) {
                    if ( line.trim().length() == 0 ) {
                        break;
                    }
                    System.out.println(line);
                }

                if (DISCONNECT.equalsIgnoreCase(command)) {
                    break;
                }
            }
        } catch (IOException e) {
            System.err.println(ERROR_WHILE_READING_INPUT);
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        FoodClient foodClient = new FoodClient();
        foodClient.startClient();
    }

}

package bg.uni.sofia.mjt.foodAnalyzer.util;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class BarcodeUtil {

    public static String getBarcodeByImage(String path) throws IOException, NotFoundException {
        MultiFormatReader reader = new MultiFormatReader();

        BufferedImage image = ImageIO.read(new File(path));
        LuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        return reader.decode(bitmap).getText();
    }

}

package bg.uni.sofia.mjt.foodAnalyzer.util;

import java.net.URI;

public class UriUtil {

    public static URI prepareUrlForSearchingFoodByKeyWord(String template, String keyWord, String apiKey) {
        keyWord = keyWord.replaceAll(" ", "%20");
        String url = String.format(template,keyWord, apiKey);
        return URI.create(url);
    }
    public static URI prepareUrlForSearchingFoodById(String template, int id, String apiKey) {
        String url = String.format(template,id, apiKey);
        return URI.create(url);
    }

}

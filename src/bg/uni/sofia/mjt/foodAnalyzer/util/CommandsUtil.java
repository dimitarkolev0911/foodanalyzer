package bg.uni.sofia.mjt.foodAnalyzer.util;

import bg.uni.sofia.mjt.foodAnalyzer.models.Command;
import bg.uni.sofia.mjt.foodAnalyzer.models.CommandEnum;

import java.util.Arrays;
import java.util.Objects;

import static bg.uni.sofia.mjt.foodAnalyzer.constants.Constants.*;

public class CommandsUtil {

    public static final String DELIMITER = " ";

    public static Command getCommandFormatByUserInput(String line) {
        Objects.requireNonNull(line, COMMAND_PARAMETER_NULL);
        String[] words = line.split(DELIMITER);
        String command = words[0];
        String argument = String.join(DELIMITER, Arrays.copyOfRange(words, 1, words.length));
        return new Command(command, argument);
    }

    public static CommandEnum getCommandFromUserInput(Command format) {
        Objects.requireNonNull(format, COMMAND_FORMAT_NULL_MESSAGE);
        String command = format.command();
        String argument = format.argument();
        switch (command) {
            case GET_FOOD:
                return  checkIfArgumentIsNotEmpty(argument) ? CommandEnum.GET_FOOD_WRONG : CommandEnum.GET_FOOD;
            case GET_FOOD_REPORT:
                return checkIfArgumentIsNotEmpty(argument) ? CommandEnum.GET_REPORT_WRONG : CommandEnum.GET_REPORT;
            case GET_FOOD_BY_BARCODE:
                if (checkIfArgumentIsNotEmpty(argument) ||
                        !(argument.startsWith(CODE_ARGUMENT) || argument.startsWith(IMG_ARGUMENT))) {
                    return CommandEnum.GET_BY_BARCODE_WRONG;
                } else if (argument.startsWith(CODE_ARGUMENT)) {
                    return CommandEnum.GET_BY_BARCODE;
                } else if (argument.startsWith(IMG_ARGUMENT)) {
                    return CommandEnum.GET_BY_BARCODE_IMAGE;
                }
                break;
            case HELP_COMMAND:
                return CommandEnum.HELP;
            case DISCONNECT:
                return CommandEnum.DISCONNECT;
        }
        return CommandEnum.DEFAULT;
    }

    private static boolean checkIfArgumentIsNotEmpty(String argument) {
        return argument == null || argument.trim().length() == 0;
    }

}

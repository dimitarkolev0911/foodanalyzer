package bg.uni.sofia.mjt.foodAnalyzer.util;

import bg.uni.sofia.mjt.foodAnalyzer.dto.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class OutputFormattingUtil {

    private static final int NUTRIENT_LENGTH = 12;
    private static final String[] foodInfoHeaderNames = new String[]{"ID", "Description", "Data Type", "Gtin Upc"};
    private static final String[] detailedSearchColumnHeaders = new String[]{"FdcId", "Product Name", "Ingredients",
            "Calories", "Proteins (g)", "Fats (g)", "Carbs (g)", "Fibers (g)"};

    public static String formatSearchingByFoodName(FoodsResult foodsResult) {
        int idMaxLength = findMaxLength(mapFoodsToId(foodsResult));
        int descriptionMaxLength = findMaxLength(mapFoodsToString(foodsResult, BaseFoodDetails::getDescription));
        int dataTypeMaxlength = findMaxLength(mapFoodsToString(foodsResult, Food::getDataType));
        dataTypeMaxlength = Math.max(dataTypeMaxlength, foodInfoHeaderNames[2].length());
        int gtinUpcMaxLength = findMaxLength(mapFoodsToString(foodsResult, Food::getGtinUpc));
        int rowWidth = calculateRowWidth(idMaxLength, descriptionMaxLength, dataTypeMaxlength, gtinUpcMaxLength);

        StringBuilder builder = new StringBuilder();

        appendHeaderRowForFood(builder, idMaxLength, descriptionMaxLength, dataTypeMaxlength, gtinUpcMaxLength);
        appendHorizontalRow(builder, rowWidth);

        for (Food f : foodsResult.getFoods()) {
            appendDataRowForFood(builder, idMaxLength, descriptionMaxLength, dataTypeMaxlength, gtinUpcMaxLength, f);
            appendHorizontalRow(builder, rowWidth);
        }
        return builder.toString();
    }

    public static String formatResultFromDetailedSearch(DetailedFoodInfo detailedFoodInfo) {
        detailedFoodInfo.setIngredients(detailedFoodInfo.getIngredients()
                .substring(detailedFoodInfo.getIngredients().indexOf(':')+2));
        int idLength = String.valueOf(detailedFoodInfo.getFdcId()).length();
        int nameLength = detailedFoodInfo.getDescription().length();
        int ingredientsLength = detailedFoodInfo.getIngredients().length();
        int rowWidth = calculateRowWidth(idLength, nameLength, ingredientsLength,
                NUTRIENT_LENGTH, NUTRIENT_LENGTH, NUTRIENT_LENGTH, NUTRIENT_LENGTH, NUTRIENT_LENGTH);

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(generateHeaderRowForDetailedInfo(idLength, nameLength, ingredientsLength));
        appendHorizontalRow(stringBuilder, rowWidth);

        stringBuilder.append(generateDataRowForDetailedInfo(idLength, nameLength,
                ingredientsLength, detailedFoodInfo));
        appendHorizontalRow(stringBuilder, rowWidth);

        return stringBuilder.toString();
    }

    public static String formatFoodOutput(Food food) {
        FoodsResult foodsResult = new FoodsResult(new Food[]{food});
        return formatSearchingByFoodName(foodsResult);
    }

    private static int findMaxLength(List<String> words) {
        return words.stream()
                .mapToInt(String::length)
                .max()
                .orElse(0);
    }

    private static int findMaxLength(int[] numbers) {
        return (int) Arrays.stream(numbers)
                .mapToLong(num -> String.valueOf(num).length())
                .max()
                .orElse(0);
    }

    private static int[] mapFoodsToId(FoodsResult foodsResult) {
        return Arrays.stream(foodsResult.getFoods())
                .mapToInt(Food::getFdcId)
                .toArray();
    }

    private static List<String> mapFoodsToString(FoodsResult foodsResult, Function<Food, String> mappingFunction) {
        return Arrays.stream(foodsResult.getFoods())
                .map(mappingFunction)
                .collect(Collectors.toList());
    }

    private static String center(String s, int size) {
        final char PADDING = ' ';
        if (s == null || size <= s.length())
            return s;

        StringBuilder sb = new StringBuilder(size);
        sb.append(String.valueOf(PADDING).repeat((size - s.length()) / 2)).append(s);
        while (sb.length() < size) {
            sb.append(PADDING);
        }
        return sb.toString();
    }

    private static String generateHeaderRowForDetailedInfo(int idLength, int nameLength, int ingredientsLength) {
        return String.format("| %" + idLength + "s | %" + nameLength + "s | %" + ingredientsLength + "s | %"
                        + NUTRIENT_LENGTH + "s | %" + NUTRIENT_LENGTH + "s | %" + NUTRIENT_LENGTH + "s | %"
                        + NUTRIENT_LENGTH + "s | %" + NUTRIENT_LENGTH + "s |",
                detailedSearchColumnHeaders[0], center(detailedSearchColumnHeaders[1], nameLength),
                center(detailedSearchColumnHeaders[2], ingredientsLength), detailedSearchColumnHeaders[3],
                detailedSearchColumnHeaders[4], detailedSearchColumnHeaders[5],
                detailedSearchColumnHeaders[6], detailedSearchColumnHeaders[7]);
    }

    private static String generateDataRowForDetailedInfo(long idLength, long nameLength, long ingredientsLength,
                                                         DetailedFoodInfo detailedFoodInfo) {
        return String.format("| %" + idLength + "s | %" + nameLength + "s | %" + ingredientsLength + "s | %"
                        + NUTRIENT_LENGTH + "s | %" + NUTRIENT_LENGTH + "s | %" + NUTRIENT_LENGTH + "s | %"
                        + NUTRIENT_LENGTH + "s | %" + NUTRIENT_LENGTH + "s |", detailedFoodInfo.getFdcId(),
                detailedFoodInfo.getDescription(), detailedFoodInfo.getIngredients(),
                getDefaultValueIfNull(detailedFoodInfo.getNutrients().getCalories()),
                getDefaultValueIfNull(detailedFoodInfo.getNutrients().getProtein()),
                getDefaultValueIfNull(detailedFoodInfo.getNutrients().getFat()),
                getDefaultValueIfNull(detailedFoodInfo.getNutrients().getCarbohydrates()),
                getDefaultValueIfNull(detailedFoodInfo.getNutrients().getFibers()));
    }

    private static String getDefaultValueIfNull(Nutrient nutrient) {
        double number = nutrient != null ? nutrient.getValue() : 0.0;
        return new BigDecimal(number).setScale(2, RoundingMode.HALF_UP).toString();
    }

    private static void appendHorizontalRow(StringBuilder builder, int rowWidth) {
        builder.append(System.lineSeparator()).append("-".repeat(rowWidth)).append(System.lineSeparator());
    }

    private static int calculateRowWidth(int... columnWidths) {
        return Arrays.stream(columnWidths).sum() + (1 + columnWidths.length * 3);
    }

    private static void appendHeaderRowForFood(StringBuilder builder, int idLength, int descriptionLength,
                                               int dataTypeLength, int gtinUpcLength) {
        builder.append(String.format("| %" + idLength + "s | %" + descriptionLength +
                        "s | %" + dataTypeLength + "s | %" + gtinUpcLength + "s |", foodInfoHeaderNames[0],
                foodInfoHeaderNames[1], foodInfoHeaderNames[2], foodInfoHeaderNames[3]));
    }

    private static void appendDataRowForFood(StringBuilder builder, int idLength, int descriptionLength,
                                             int dataTypeLength, int gtinUpcLength, Food food) {
        builder.append(String.format("| %" + idLength + "d | %" + descriptionLength +
                        "s | %" + dataTypeLength + "s | %" + gtinUpcLength + "s |",
                food.getFdcId(), center(food.getDescription(), food.getDescription().length()), food.getDataType(),
                food.getGtinUpc()));
    }

}

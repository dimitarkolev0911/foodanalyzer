package bg.uni.sofia.mjt.foodAnalyzer.constants;

import java.io.File;

public class Constants {

    public static final String API_KEY = "uwiSTMQvUdMIJi7kD9P07BcAcaC8Px7NnNKq1cU2";
    public static final String SERVER_HOST = "localhost";

    //Errors in server/client
    public static final String ERROR_WITH_INITIALIZING_THE_SERVER = "Error with initializing the server!";
    public static final String NOTHING_TO_READ_MESSAGE = "Nothing to read, will close channel!";
    public static final String ERROR_WHILE_RUNNING_THE_SERVER = "Error while running the server!";
    public static final String ERROR_IN_ACCEPTING_SOCKET = "Error in accepting a new socket channel";
    public static final String ERROR_WHILE_READING_WRITING_IN_BUFFER = "Error in reading or writing to buffer";
    public static final String ERROR_WHILE_READING_INPUT = "Error when trying to get input from the client socket";
    public static final String BUFFER_OVERLOAD_MESSAGE = "The message should be no more than %d bytes longer";

    //Error messages
    public static final String COMMAND_FORMAT_NULL_MESSAGE = "The command format cannot be null";
    public static final String COMMAND_PARAMETER_NULL = "Cannot get command from null parameter";
    public static final String IO_EXCEPTION_MESSAGE = "There was an error while trying to write to cache!";
    public static final String HTTP_RESPONSE_MESSAGE = "There was an error while sending the request!";
    public static final String NOT_FOUND_MESSAGE = "The path to the barcode image is invalid!";

    //Commands
    public static final String GET_FOOD = "get-food";
    public static final String GET_FOOD_REPORT = "get-food-report";
    public static final String GET_FOOD_BY_BARCODE = "get-food-by-barcode";
    public static final String HELP_COMMAND = "help";
    public static final String DISCONNECT = "disconnect";
    public static final String CODE_ARGUMENT = "--code=";
    public static final String IMG_ARGUMENT = "--img=";

    //Messages for the client
    public static final String NO_SUCH_PRODUCT_MESSAGE = "There is no such product in our database!";
    public static final String UNKNOWN_COMMAND = "Unknown command!";
    public static final String GET_INFO_USAGE = "get-info usage: get-info <food_name>";
    public static final String GET_FOOD_REPORT_USAGE = "get-food-report usage: get-food-report <food_fdcId>";
    public static final String NO_RESULTS = "There were no products with this name";
    public static final String GET_FOOD_BY_BARCODE_USAGE =
            "get-food-by-barcode usage: get-food-by-barcode --code=<gtinUpc_code>|--img=<path_to_image>";
    public static final String HELP_MESSAGE = "get-food <food-name>" + System.lineSeparator() +
            "get-food-report <food_fdc_id>" + System.lineSeparator() +
            "get-food-by-barcode --code=<gtinUpc_code>|--img=<barcode_image_file>";
    public static final String DISCONNECTING = "Disconnecting...";

    //Client "UI"
    public static final String PROMPT = "> ";

    //Files
    public static final String CACHE_DIRECTORY = "resources" + File.separator + "cache";
    public static final String CACHE_TEST_DIRECTORY = "resources" + File.separator + "test" + File.separator + "cache";

}
